from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    title=models.CharField(max_length=200)

    def __str__(self):
        return self.title

class News(models.Model):
    category    =      models.ForeignKey(Category,on_delete=models.CASCADE)
    title       =      models.CharField(max_length=200)
    content     =      models.TextField(blank=False,null=False)
    image       =      models.ImageField(upload_to='news')
    date        =      models.DateField()
    def __str__(self):
        return self.title



