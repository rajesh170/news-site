from django.urls import path
from myapp.views import *

urlpatterns = [
    path('',HomeView.as_view(),name='home'),
    path('home/',HomeView.as_view(),name='home'),
    path('sports/',SportsView.as_view(),name='sports'),
    path('international/',InternationalView.as_view(),name='international'),
    path('politics/',PoliticsView.as_view(),name='politics'),
    path('economic/',EconomicView.as_view(),name='economic'),

    path('register/',SignupView.as_view()),
    path('login/',LoginView.as_view()),
    path('logout/',LogoutView.as_view()),
]
