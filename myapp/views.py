from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/login/")

        return super().dispatch(request, *args, **kwargs)


class HomeView(LoginRequiredMixin,ListView):
    template_name='home.html'
    queryset=News.objects.all()
    context_object_name='allnews'

class SportsView(LoginRequiredMixin,ListView):
    template_name='sports.html'
    queryset=News.objects.filter(category_id=1)
    context_object_name='allsports'

class InternationalView(LoginRequiredMixin,ListView):
    template_name='international.html'
    queryset=News.objects.filter(category_id=4)
    context_object_name='allint'

class PoliticsView(LoginRequiredMixin,ListView):
    template_name='politics.html'
    queryset=News.objects.filter(category_id=5)
    context_object_name='allpol'

class EconomicView(LoginRequiredMixin,ListView):
    template_name='economic.html'
    queryset=News.objects.filter(category_id=2)
    context_object_name='alleco'

class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/home/"

    def form_valid(self, form):
        x = form.cleaned_data["username"]
        y = form.cleaned_data["password"]
        usr = authenticate(username=x, password=y)
        if usr is not None:
            login(self.request, usr)

        else:
            return render(self.request, 'login.html',
                          {
                              "error": "Invalid Credential",
                              "form": form
                          })

        return super().form_valid(form)

class SignupView(FormView):                                         
    template_name='signup.html'
    form_class=SignupForm
    success_url='/'

    def form_valid(self,form):
        x=form.cleaned_data["username"]
        e=form.cleaned_data["email"]
        p=form.cleaned_data["password"]

        usr=User.objects.create_user(x,e,p)
        login(self.request,usr)

        return super().form_valid(form)



class LogoutView(View):
    def get(self, request):
        logout(request)

        return redirect('/login/')



