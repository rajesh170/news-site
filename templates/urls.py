from django.contrib import admin
from django.urls import path
from blogapp.views import *
urlpatterns = [
    
    path('',BlogTableView.as_view(),name='table'),
    path('home/',BlogTableView.as_view(),name='table'),
    path('contact/',ContactView.as_view(),name='contact'),
    path('about/',AboutView.as_view(),name='about'),
    path('blogs/',BlogListView.as_view(),name='bloglist'),
    path('blogs/<int:pk>/detail/',BlogDetailView.as_view(),name='blogdetail'),
    path('blogs/create/',BlogCreateView.as_view(),name='createblog'),
    path('blogs/<int:pk>/update/',BlogUpdateView.as_view(),name='blogupdate'),
    path('blogs/<int:pk>/delete/',BlogDeleteView.as_view(),name='blogdelete'),
    path('register/',SignupView.as_view()),
    path('login/',LoginView.as_view()),
    path('logout/',LogoutView.as_view()),


    
    
    



]
